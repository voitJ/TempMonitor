# TempMonitor

Check the temperature and humidity of Barletta City!

# Local test

```
cd TempMonitor/it/controller
export FLASK_APP=SensorController.py
lask run --host=0.0.0.0
```

# Webservice test

```
testprojectpi.ddns.net:51413/
```