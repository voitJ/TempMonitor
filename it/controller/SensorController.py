'''
Created on 28 gen 2018

@author: Vito Rizzi
'''
from flask import Flask
import json
from it.utility import SensorDHTConnector
app = Flask(__name__)

@app.route('/')
def hello_world():
    sensor = SensorDHTConnector.getSensor()
    return json.dumps(sensor.jDefault())