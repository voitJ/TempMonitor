import Adafruit_DHT
from it.model.Sensor import Sensor

def getSensor():
        humidity, temperature = Adafruit_DHT.read_retry(11, 4)
        s =  Sensor(temperature, humidity)
        return s
